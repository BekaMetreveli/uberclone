package com.example.uber

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.uber.databinding.ActivityViewRequestsBinding
import com.parse.*
import kotlin.math.round

class ViewRequestsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityViewRequestsBinding

    private lateinit var adapter: RequestsRecyclerViewAdapter
    private var requests = mutableListOf<String>()
    private var locations = mutableListOf<ParseGeoPoint>()
    private var usernames = mutableListOf<String>()
    private var currentLocation: Location? = null

    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewRequestsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        title = "Nearby Requests"

        setAdapter()

        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationListener = LocationListener { p0 ->
            updateRecyclerView(p0)
            currentLocation = p0
            ParseUser.getCurrentUser().put("location", ParseGeoPoint(p0.latitude, p0.longitude))
            ParseUser.getCurrentUser().saveInBackground()

        }

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        } else {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                0,
                0f,
                locationListener
            )
            val lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (lastKnownLocation != null) {
                updateRecyclerView(lastKnownLocation)
                currentLocation = lastKnownLocation
            }
        }
    }

    private fun setAdapter() {
        requests.clear()
        locations.clear()
        usernames.clear()
        requests.add("Getting Nearby requests...")
        locations.add(ParseGeoPoint(0.0, 0.0))
        usernames.add("abc")
        adapter = RequestsRecyclerViewAdapter(requests, locations, null, usernames)
        binding.requestsRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.requestsRecyclerView.adapter = adapter

    }

    private fun updateRecyclerView(location: Location) {
        val query = ParseQuery.getQuery<ParseObject>("Request")
        val geoPointLocation = ParseGeoPoint(location.latitude, location.longitude)

        query.whereNear("location", geoPointLocation)
        query.whereDoesNotExist("driverUsername")
        query.limit = 10
        query.findInBackground { objects, e ->
            if (e == null) {
                requests.clear()
                locations.clear()
                usernames.clear()
                if (objects.size > 0) {
                    for (obj in objects) {
                        val distanceInMiles =
                            geoPointLocation.distanceInMilesTo(obj.get("location") as ParseGeoPoint)
                        val distanceOneDP = round(distanceInMiles * 10) / 10
                        requests.add("$distanceOneDP miles")
                        locations.add(obj.get("location") as ParseGeoPoint)
                        usernames.add(obj.getString("username") as String)
                    }
                } else {
                    requests.add("No Active Requests Nearby")
                    locations.add(ParseGeoPoint())
                    usernames.add("abc")
                }
                adapter.setAdapterData(requests, locations, currentLocation, usernames)
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        0,
                        0f,
                        locationListener
                    )

                    val lastKnownLocation =
                        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    if (lastKnownLocation != null) {
                        updateRecyclerView(lastKnownLocation)
                        currentLocation = lastKnownLocation
                    }
                }
            }
        }
    }

}