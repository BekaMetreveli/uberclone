package com.example.uber

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.uber.databinding.ActivityRiderBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.parse.*
import kotlin.math.round

class RiderActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener

    private lateinit var binding: ActivityRiderBinding
    private var isRequestActive = false
    private val handler = Handler()
    private var isDriverActive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRiderBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        val query = ParseQuery<ParseObject>("Request")
        query.whereEqualTo("username", ParseUser.getCurrentUser().username)
        query.findInBackground { objects, e ->
            if (e == null) {
                if (objects.size > 0) {
                    isRequestActive = true
                    binding.callUberButton.text = "Cancel Uber"

                    checkForUpdates()
                }
            }
        }

        binding.callUberButton.setOnClickListener {
            if (isRequestActive) {
                val query = ParseQuery<ParseObject>("Request")
                query.whereEqualTo("username", ParseUser.getCurrentUser().username)
                query.findInBackground { objects, e ->
                    if (e == null) {
                        if (objects.size > 0) {
                            isRequestActive = false
                            binding.callUberButton.text = "Call an Uber"
                            for (obj in objects) {
                                obj.deleteInBackground()
                            }
                        }
                    }
                }
            } else {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        0,
                        0f,
                        locationListener
                    )

                    val lastKnownLocation =
                        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    if (lastKnownLocation != null) {
                        val request = ParseObject("Request")
                        request.put("username", ParseUser.getCurrentUser().username)

                        val parseGeoPoint =
                            ParseGeoPoint(lastKnownLocation.latitude, lastKnownLocation.longitude)
                        request.put("location", parseGeoPoint)
                        request.saveInBackground { e ->
                            if (e == null) {
                                binding.callUberButton.text = "Cancel Uber"
                                isRequestActive = true

                                checkForUpdates()
                            }
                        }
                    } else {
                        Toast.makeText(
                            this,
                            "Could not find location. Try Again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

        }

        binding.logOutButton.setOnClickListener {
            ParseUser.logOut()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun checkForUpdates() {
        val query = ParseQuery.getQuery<ParseObject>("Request")
        query.whereEqualTo("username", ParseUser.getCurrentUser().username)
        query.whereExists("driverUsername")

        query.findInBackground { objects, e ->
            if (e == null && objects.size > 0) {
                isDriverActive = true
                val query2 = ParseUser.getQuery()
                query2.whereEqualTo("username", objects[0].getString("driverUsername"))
                query2.findInBackground { objects2, ex ->
                    if (ex == null && objects2.size > 0) {
                        val driverLocation = objects[0].getParseGeoPoint("location")
                        if (ContextCompat.checkSelfPermission(
                                this,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            val lastKnownLocation =
                                locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                            if (lastKnownLocation != null) {
                                val userLocation = ParseGeoPoint(
                                    lastKnownLocation.latitude,
                                    lastKnownLocation.longitude
                                )
                                val distanceInMiles =
                                    driverLocation!!.distanceInMilesTo(userLocation)
                                if (distanceInMiles < 0.01) {
                                    binding.infoTextView.text = "Your driver is here"
                                    val query3 = ParseQuery.getQuery<ParseObject>("Request")
                                    query3.whereEqualTo("username", ParseUser.getCurrentUser().username)
                                    query3.findInBackground { objects3, exe ->
                                        if (exe == null && objects3.size > 0) {
                                            for (obj in objects3) {
                                                obj.deleteInBackground()
                                            }
                                        }
                                    }

                                    handler.postDelayed(object : Runnable {
                                        override fun run() {
                                            binding.infoTextView.text = ""
                                            binding.callUberButton.visibility = View.VISIBLE
                                            binding.callUberButton.text = "Call an Uber"
                                            isRequestActive = false
                                            isDriverActive = false
                                        }

                                    }, 5000)
                                } else {
                                    val distanceOneDP = round(distanceInMiles * 10) / 10
                                    binding.infoTextView.text =
                                        "Driver is $distanceOneDP miles away"

                                    val driverLocationLatLng = LatLng(
                                        driverLocation.latitude,
                                        driverLocation.longitude
                                    )
                                    val requestLocation = LatLng(
                                        userLocation.latitude,
                                        userLocation.longitude
                                    )

                                    val markers = mutableListOf<Marker>()
                                    mMap.clear()
                                    markers.add(
                                        mMap.addMarker(
                                            MarkerOptions().position(driverLocationLatLng)
                                                .title("Driver Location")
                                        )
                                    )
                                    markers.add(
                                        mMap.addMarker(
                                            MarkerOptions().position(requestLocation)
                                                .title("Your Location").icon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_BLUE
                                                    )
                                                )
                                        )
                                    )

                                    val builder = LatLngBounds.Builder()
                                    for (marker in markers) {
                                        builder.include(marker.position)
                                    }
                                    val bounds = builder.build()

                                    val padding = 200
                                    val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                                    mMap.animateCamera(cu)

                                    binding.callUberButton.visibility = View.INVISIBLE

                                    handler.postDelayed(object : Runnable {
                                        override fun run() {
                                            checkForUpdates()
                                        }

                                    }, 2000)
                                }
                            }

                        }
                    }
                }


            }
            handler.postDelayed(object : Runnable {
                override fun run() {
                    checkForUpdates()
                }

            }, 2000)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationListener = object : LocationListener {
            override fun onLocationChanged(p0: Location) {
                updateMap(p0)
            }
        }


        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        } else {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                0,
                0f,
                locationListener
            )
            val lastKnownLocation =
                locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (lastKnownLocation != null) {
                updateMap(lastKnownLocation)
            }
        }
    }

    private fun updateMap(location: Location) {
        if (isDriverActive) {
            val userLocation = LatLng(location.latitude, location.longitude)
            mMap.clear()
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15f))
            mMap.addMarker(MarkerOptions().position(userLocation).title("Your Location"))
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        0,
                        0f,
                        locationListener
                    )

                    val lastKnownLocation =
                        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    if (lastKnownLocation != null) {
                        updateMap(lastKnownLocation)
                    }
                }
            }
        }
    }

}