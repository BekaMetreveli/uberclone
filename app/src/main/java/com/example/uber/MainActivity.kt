package com.example.uber

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.uber.databinding.ActivityMainBinding
import com.parse.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if (ParseUser.getCurrentUser() == null) {
            ParseAnonymousUtils.logIn { user, e ->
                if (e == null) {
                    //Success
                    Log.i("Anonymous Log in", "Success")
                } else {
                    //Failure
                    Log.i("Anonymous Log in", "Failure")
                }
            }
        } else {
            if (ParseUser.getCurrentUser().get("riderOrDriver") != null) {
                Log.i("Redirecting as ", ParseUser.getCurrentUser().get("riderOrDriver") as String)

                redirectActivity()
            }
        }

        binding.getStartedButton.setOnClickListener {
            var userType = "rider"
            if (binding.userTypeSwitch.isChecked) {
                userType = "driver"
            }
            ParseUser.getCurrentUser().put("riderOrDriver", userType)
            ParseUser.getCurrentUser().saveInBackground { redirectActivity() }
        }
    }

    private fun redirectActivity() {
        if (ParseUser.getCurrentUser().get("riderOrDriver") == "rider") {
            val intent = Intent(this, RiderActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, ViewRequestsActivity::class.java)
            startActivity(intent)
        }
    }
}