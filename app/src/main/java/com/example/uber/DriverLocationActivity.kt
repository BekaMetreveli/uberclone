package com.example.uber

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import com.example.uber.databinding.ActivityDriverLocationBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.parse.*

class DriverLocationActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private lateinit var binding: ActivityDriverLocationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDriverLocationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.acceptRequestButton.setOnClickListener {
            val query = ParseQuery.getQuery<ParseObject>("Request")
            query.whereEqualTo("username", intent.getStringExtra("username"))
            query.findInBackground { objects, e ->
                if (e == null) {
                    if (objects.size > 0) {
                        for (obj in objects) {
                            obj.put("driverUsername", ParseUser.getCurrentUser().username)
                            obj.saveInBackground { ex ->
                                if (ex == null) {

                                    val directionsIntent: Intent = Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse(
                                            "http://maps.google.com/maps?saddr=" + intent.getDoubleExtra(
                                                "driverLatitude",
                                                0.0
                                            ).toString() + "," + intent.getDoubleExtra(
                                                "driverLongitude",
                                                0.0
                                            ).toString() + "&daddr=" + intent.getDoubleExtra(
                                                "requestLatitude",
                                                0.0
                                            ).toString() + "," + intent.getDoubleExtra(
                                                "requestLongitude",
                                                0.0
                                            )
                                        )
                                    )
                                    directionsIntent.setPackage("com.google.android.apps.maps")
                                    startActivity(directionsIntent)
                                }
                            }
                        }
                    }
                }
            }
        }

        binding.root.viewTreeObserver.addOnGlobalLayoutListener {
            val driverLocation = LatLng(
                intent.getDoubleExtra("driverLatitude", 0.0),
                intent.getDoubleExtra("driverLongitude", 0.0)
            )
            val requestLocation = LatLng(
                intent.getDoubleExtra("requestLatitude", 0.0),
                intent.getDoubleExtra("requestLongitude", 0.0)
            )

            val markers = mutableListOf<Marker>()
            markers.add(
                mMap.addMarker(
                    MarkerOptions().position(driverLocation).title("Your Location")
                )
            )
            markers.add(
                mMap.addMarker(
                    MarkerOptions().position(requestLocation).title("Request Location").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                )
            )

            val builder = LatLngBounds.Builder()
            for (marker in markers) {
                builder.include(marker.position)
            }
            val bounds = builder.build()

            val padding = 200
            val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
            mMap.animateCamera(cu)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }
}