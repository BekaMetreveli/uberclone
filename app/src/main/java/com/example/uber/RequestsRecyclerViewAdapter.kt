package com.example.uber

import android.content.Intent
import android.location.Location
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.uber.databinding.RequestLayoutBinding
import com.parse.ParseGeoPoint

class RequestsRecyclerViewAdapter(
    private var requests: MutableList<String>,
    private var locations: MutableList<ParseGeoPoint>,
    private var driverLocation: Location?,
    private var usernames: MutableList<String>
) :
    RecyclerView.Adapter<RequestsRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RequestsRecyclerViewAdapter.ViewHolder {
        val itemBinding =
            RequestLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }


    override fun onBindViewHolder(holder: RequestsRecyclerViewAdapter.ViewHolder, position: Int) {
        return holder.onBind(requests[position], locations[position], usernames[position])
    }

    override fun getItemCount(): Int = requests.size


    fun setAdapterData(adapterItems: MutableList<String>, locations: MutableList<ParseGeoPoint>, driverLocation: Location?, usernames: MutableList<String>) {
        this.requests = adapterItems
        this.locations = locations
        this.driverLocation = driverLocation
        this.usernames = usernames
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(private val itemBinding: RequestLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun onBind(request: String, location: ParseGeoPoint, username: String) = with(itemBinding) {
            requestTextView.text = request

            root.setOnClickListener {
                val intent = Intent(itemView.context, DriverLocationActivity::class.java)
                intent.putExtra("requestLatitude", location.latitude)
                intent.putExtra("requestLongitude", location.longitude)
                intent.putExtra("driverLatitude", driverLocation!!.latitude)
                intent.putExtra("driverLongitude", driverLocation!!.longitude)
                intent.putExtra("username", username)
                ContextCompat.startActivity(itemView.context, intent, null)
            }
        }
    }
}