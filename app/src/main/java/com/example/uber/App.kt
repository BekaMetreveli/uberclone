package com.example.uber

import android.app.Application
import com.parse.Parse
import com.parse.ParseACL

class App : Application() {
    //// Reference: https://docs.parseplatform.org/android/guide/#the-parseobject
    override fun onCreate() {
        super.onCreate()

        // Add your initialization code here
        Parse.initialize(
            Parse.Configuration.Builder(applicationContext)
                .applicationId("myappID") // if defined
                .clientKey("kDRUdNTa1v9w") // Add your Server IP Addresses
                .server("http://3.17.109.9/parse/") // Add Your Server IP. Do not add "PORT Number" such as ip:80 or ip:1337
                .build()
        )

        val defaultACL = ParseACL()
        defaultACL.publicReadAccess = true
        defaultACL.publicWriteAccess = true
        ParseACL.setDefaultACL(defaultACL, true)
    }
}
